# DockerCompose-IoT-Basics

Hysteresis temperature control:

<img src="./grafana_tempmonitor.png" width="490" height="230" />

prerequisites:

- docker latest
- docker-compose latest
- data source
  
    I used my termostat. Build a microcontroller unit, use pubsubclient library in Arduino
    Or just send dummy data from python (paho-mqtt) or use the terminal with mosquitto_pub

### ToDo

- [x] readme
- [x] mqtt description
  - [ ] test mqtt this way

## Smarthome Communication Metrics and Visualisation - Smarthome-CMV

MQTT Telegraf Influxdb Grafana
This is just a summary of steps of how I made it work.

further explanation and sources:

[A similar description: lucassardois](https://lucassardois.medium.com/handling-iot-data-with-mqtt-telegraf-influxdb-and-grafana-5a431480217)

[A similar description with chronograf: blog.anoff.io](https://blog.anoff.io/2020-12-run-influx-on-raspi-docker-compose/)

[An old description with Arduino code, gabrieltanner.org](https://gabrieltanner.org/blog/grafana-sensor-visualization)

[A grafana docker-compose tutorial, blog.anoff.io](https://blog.anoff.io/2021-01-howto-grafana-on-raspi/)


#### System

RaspberryPi 4
Linux raspberrypi 5.10.63-v7l+ #1459 SMP Wed Oct 6 16:41:57 BST 2021 armv7l GNU/Linux\
docker version: 20.10.14\
docker-compose version: 1.29.2

### How-to

pull docker images:
(influxdb latest for arm/v7 is 1.8.10
raspbian is 32bit, altough cpu is 64bit... its a raspbian thing.)

- **docker pull eclipse-mosquitto:2.0.11 telegraf influxdb:1.8.10 grafana/grafana:latest**

- create folder structure (files are created in the steps below)

```markdown
├── docker-compose.yml
│ 
├── .env                                   # for influxdb and grafana username pw.
│ 
├── grafana                                # volume
│   ├── data                               # you have to chown it to 472 later, see below, leave empty        
│   ├── grafana.ini                        # config for grafana, create later
│   └── provisioning                       # leave it empty
│ 
├── influxdb                               # volume
│   ├── data                               # influxdb working directory (where your actual data is stored)
│   ├── influxdb.conf                      # create later, see below
│   └── init
│       └── create-sensors.iql             # initial script for creating database
│ 
├── mqtt                                   # volume
│   ├── config
│   │   ├── mosquitto.conf                 # create this later, see below
│   │   └── pwfile                         # later below
│   ├── data                               # leave it empty
│   └── log                                # leave it empty
│      
├── telegraf                               # volume
│   └── telegraf.conf                      # create later, see below
```



- **docker run --rm influxdb:1.8.10 influxd config > influxdb.conf**

    - set **auth-enabled = true** in influxdb.conf http section. <br />so that our telegraf container has to authenticate itself with a username/pw. 
    - create the .env file in the influxdb folder:
        <pre><code>
        INFLUXDB_USERNAME=admin
        INFLUXDB_PASSWORD=superSECRETinfluxPASSWORD
        </pre></code>

- **docker run --rm telegraf telegraf config > telegraf.conf**

    edit:

    - the hostname in the [agent] section to your $HOSTNAME

        hostname = "${HOSTNAME}"

    - the influxdb plugin in OUTPUT PLUGINS section:

            [[outputs.influxdb]]

            urls = ["http://influxdb-scmv:8086"]  # This has to match container name.
            database = "sensors"
            username = "telegraf"  #your choice
            password = "telegraf"
            skip_database_creation = true      #since we will create it with init script.

    - the mqtt plugin in INPUT PLUGINS section:

            [[inputs.mqtt_consumer]]

            servers = ["tcp://localhost:1883"]
            topics = ["Livingroom/Data/Temp"] #your choice, I measure temp with an esp32 and a bme280
            username = "mqtt-broker-username" #your choice, if you set up a passwordless broker leave empty.
            password = "mqtt-broker-password"
            client_id = "telegraf_client"     #your choice
            data_format = "value"
            data_type = "float"

- create init script for sensors database and user.

    - create-sensors.iql

        <pre><code>
        CREATE DATABASE sensors
        CREATE USER username WITH PASSWORD 'password'  #which you have defined in telegraf.conf
        GRANT ALL ON sensors TO username
        </code></pre>

- **docker run --rm --entrypoint /bin/bash grafana/grafana:latest -c 'cat $GF_PATHS_CONFIG' > grafana.ini**

    run this, so you have a config file from grafana too, if you need to edit it.

    - edit the .env file with grafana username and pw:
  
        <pre><code>
        INFLUXDB_USERNAME=yourusername
        INFLUXDB_PASSWORD=yourpasswd
        GF_SECURITY_ADMIN_USER=yourusername
        GF_SECURITY_ADMIN_PASSWORD=yourpasswd
        </code></pre>

    - **chown 472:472 YOURPATH/grafana/data**

        Make sure your local directory is owned by the default Grafana user ID 472:472<br />
        There are others methods to enable grafana to use the volume, but this one worked for me.

- **docker network create --gateway 172.20.0.1 --subnet 172.20.0.0/16 smarthome**  #name is your choice of course

    I created a smarthome docker network, by default its a bridge driver type.
    This way, my others projects can use this network.
    By default docker-compose creates its own network based on the project folder
    for your services described in the docker-compose.yml. This way, Its gonna be an external network we use.

- copy or edit docker-compose.yml

- spin up the services with **docker-compose up -d**, include the mqtt part too. 

    There will be a network and volumes.

    **docker network list**<br />
    **docker volume list**

    note them.
    
    - create mosquitto.conf and edit:
    <p><pre><code>
    persistence true
    persistence_location /mosquitto/data/
    log_dest file /mosquitto/log/mosquitto.log
    </code></pre><p>
    - open the mqtt container shell
 
    **docker exec -it 'mqtt-container' sh**

    - create a user with password.
     
        <code>mosquitto_passwd -c /mosquitto/config/mosquitto.passwd 'username'</code>

    - it will promt for passwd, exit the container.
    - finish editing the mosquitto.conf file:
        <pre><code>
        persistence true
        persistence_location /mosquitto/data/
        log_dest file /mosquitto/log/mosquitto.log
        
        password_file /mosquitto/config/mosquitto.passwd
        allow_anonymous false
        
        listener 1883
        listener 9001
        protocol websockets
        </code></pre>

    - test it: (sudo apt install mosquitto-client, two terminals or tmux)

            mosquittp_sub -h "localhost" -u "username" -P "password" -t "test"
            mosquitto_pub -h "localhost" -u "username" -P "password" -t "test" -m "hello world"

    if all is well, open port 3000 on your machine from a browser
    - go to data sources and fill your influxdb auth info

        If its good, it will promt you, that it sees the data-source.<br />
    See a tutorial for grafana, but if you just add a basic dashboard and a panel<br />
    You should be able to select the mqtt_consumer default table from the default query


### debug
for debuging purposes you can just 'docker-compose up' without the -d (d indicating detached mode),
so you see every detail of your running containers, and search for errors. <br />
It could be a good idea, to delete volumes if you fail at somepoint.<br />
both the created files from the containers, and with docker volume rm 'volume name'

### standalone mqtt broker
If you are looking for this, just remove all other parts from the docker-compose file.<br />
perform the mqtt config steps.

## Authors and acknowledgment
Created from various tutorials found online, this is just a summary for myself and maybe others.

